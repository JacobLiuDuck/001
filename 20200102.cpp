#include <bits/stdc++.h>
using namespace std;
int m[256] = {};
int x[256] = {')', 0x06, 0x16, 'O', '+', '5', '0', 0x1e, 0x51, 0x1b, 0x5b, 0x14, 0x4b, 0x8, 0x5d, 0x2b, 0x5c, 0x10, 0x06, 0x06, 0x18, 0x45, 0x51, 0x00, 0x5D, 0x00};
int main() {
    string str = "You have now entered the Duck Web, and you're in for a honkin' good time.\nCan you figure out my trick?";
    int sz = str.size();
    for (int i = 0; i < str.size(); i++) {
        m[i] = str[i];
    }
    for (int i = 0; i < sz; i++) {
        char ch = m[i] ^ x[i];
        if (isprint(ch)) {
            cout << ch;
        } else {
            cout << "_";
        }
    }
    cout << endl;
    return 0;
}
/*
db  29h ; )
.rodata:08048859                 db    6
.rodata:0804885A                 db  16h
.rodata:0804885B                 db  4Fh ; O
.rodata:0804885C                 db  2Bh ; +
.rodata:0804885D                 db  35h ; 5
.rodata:0804885E                 db  30h ; 0
.rodata:0804885F                 db  1Eh
.rodata:08048860                 db  51h ; Q
.rodata:08048861                 db  1Bh
.rodata:08048862                 db  5Bh ; [
.rodata:08048863                 db  14h
.rodata:08048864                 db  4Bh ; K
.rodata:08048865                 db    8
.rodata:08048866                 db  5Dh ; ]
.rodata:08048867                 db  2Bh ; +
.rodata:08048868                 db  5Ch ; \
.rodata:08048869                 db  10h
.rodata:0804886A                 db    6
.rodata:0804886B                 db    6
.rodata:0804886C                 db  18h
.rodata:0804886D                 db  45h ; E
.rodata:0804886E                 db  51h ; Q
.rodata:0804886F                 db    0
.rodata:08048870                 db  5Dh ; ]
.rodata:08048871                 db    0
*/